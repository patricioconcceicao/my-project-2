# Apresentação

Olá, meu nome é Patricio Conceição e seja bem vindo ao meu Projeto. 

## Aboradangens com simulaçoes de experimentos virtuais no PIBID e outras metodologias

1. Resumo

  Esse projeto desenvolvido entre 2018-2020 visa uma nova forma de abordar experimentos no contexto escolar. O foco do projeto era trazer uma forma diferente de trabalhar o conteudo de fisica no ensino médio diante da nova realidade. Diante disso,  é importante trazer esses temas de forma reflexiva nos projetos implementados e na
metodologia empregada no colégio Estadual Duque de Caxias e nas dificuldades
encontradas no âmbito escolar. Com isso, retiramos como conhecimento essas
experiencias adquiridas e consequentemente poderá servir como base no futuro
para que possam por melhoras ao ser aplicado no ensino de física. Sobre esse
viés, a disciplina de Física tradicionalmente apresenta seu enfoque
fundamentando-se em aspectos essencialmente teóricos. Isso pode ser
facilmente observado mediante a análise dos livros didáticos da disciplina que
são utilizados nas escolas, os quais se concentram basicamente em conceitos
matemáticos e exercícios de fixação e na ausência de laboratórios didáticos na
escola faz com que a necessidade de aplicar uma nova forma de abordar o
conteúdo e estimular o interesse dos alunos pela física Em uma abordagem
voltada para o uso de laboratório didático virtual demonstrativo que tinha como
intuito visar diminuir as dificuldades dos alunos do Ensino Médio na assimilação
de conteúdo desta disciplina com a possibilidade de gerar o interesse e estímulo
para a aprendizagem dos conteúdos, e também, com o intuito de incentivar a
investigação científica e trazer para os alunos uma nova forma de aprendizagem
através da investigação propormos a partir do laboratório didático investigativo
que os mesmos executem um experimento da forma que eles achassem melhor
e observassem os resultados obtidos. Assim, a vivencia nos fez perceber a
importância da abordagem do ensino de Física através de experimentações no
Ensino Médio como estratégia que possibilita a aprendizagem da disciplina de
forma prática e concreta. E como bolsistas a vivencia no âmbito escolar nos trás
uma nova forma de visão sobre o atual cenário do ensino de física.


2.Introdução ao PIBID

3. Descrição das atividades

3. Reflexão e desafios 

4. Referencias 
